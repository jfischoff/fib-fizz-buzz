import FibFizzBuzz.Main hiding (main)
import Test.Hspec
import Test.QuickCheck
import Control.Monad (forM_)

divideOutNs :: Integer -> Integer -> Integer
divideOutNs n x
  | x == 0 = x
  | x `mod` n == 0 = divideOutNs n $ x `div` n
  | otherwise = x

main :: IO ()
main = hspec $ do
  describe "fizzBuzz" $ do
    it "zero returns FizzBuzz" $ do
      fizzBuzz 0 0 == "FizzBuzz"
    it "returns BuzzFizz for primes indices" $ do
      forM_ fibinocciPrimes $ \primeIndex ->
        fizzBuzz primeIndex (fibs !! fromIntegral primeIndex) `shouldBe` "BuzzFizz"
    it "returns FizzBuzz for multiples of 15" $ property $ \x ->
      fizzBuzz 0 (x * 15) == "FizzBuzz"
    it "returns Fizz for multiples of 3" $ property $ \(NonZero x) ->
      -- divide out the 5s so we don't Fizz
      fizzBuzz 0 (divideOutNs 5 x * 3) == "Buzz"
    it "returns Fizz for multiples of 5" $ property $ \(NonZero x) ->
      -- divide out the 3s so we don't Buzz
      fizzBuzz 0 (divideOutNs 3 x * 5) == "Fizz"
