# fib-fizz-buzz

The is an efficent version of the spec described here: https://github.com/swift-nav/screening_questions/blob/master/questions.md#swift-navigation-application-questions

It operates over very large numbers but doesn't cover the range of all possible integers and instead covers the Fibonacci indices between 0 and 104911. The 104911th fibonacci number is the largest known Fibonacci prime. Determining if a number with 10s of thousands of digits is prime is considerable intractable and limited range allows the program to work efficently.
