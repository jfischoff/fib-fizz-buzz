{-# LANGUAGE MultiWayIf #-}
module FibFizzBuzz.Main where
import Control.Monad (zipWithM_)

-- There are only handleful of known Fibinocci primes
-- Primes are copied from here: https://oeis.org/A005478
-- Anything beyond this is considered intractable
-- Indices start at zero
fibinocciPrimes :: [Integer]
fibinocciPrimes = [3, 4, 5, 7, 11, 13, 17, 23, 29, 43, 47, 83, 131, 137, 359,
  431, 433, 449, 509, 569, 571, 2971, 4723, 5387, 9311, 9677, 14431, 25561,
  30757, 35999, 37511, 50833, 81839, 104911]

fibs :: [Integer]
fibs = 0 : scanl (+) 1 fibs

-- This fib fizz buzzes the first 104912 Fibinocci numbers. The numbers get
-- very large very quickly, the growth is exponential. Anything beyond
-- 104912th number is considered too big to worry about.
main :: IO ()
main = zipWithM_ (\i n -> putStrLn $ fizzBuzz i n) [0 .. 104911] fibs

fizzBuzz :: Integer -> Integer -> String
fizzBuzz index nth =
  -- This could be written with out the explicit 15 mod test, but it makes the
  -- code harder to to read
  -- The order of these tests is significant. We have to test the composite
  -- conditions first
  if | nth `mod` 15 == 0 -> "FizzBuzz"
     | any (== index) fibinocciPrimes -> "BuzzFizz"
     | nth `mod` 5 == 0 -> "Fizz"
     | nth `mod` 3 == 0 -> "Buzz"
     | otherwise -> show nth
